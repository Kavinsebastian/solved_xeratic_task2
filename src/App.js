import { useState } from 'react';
import './App.css';
import Question1 from './Components/Question1'
import Question2 from './Components/Question2'
import Question3 from './Components/Question3'
import Question4 from './Components/Question4'


function App() {
  const [page, setPage] = useState(1)
  const [answer1 , setAnswer1] = useState('')
  const [fixAnswer1, setFixAnswer1] = useState('')
  const [answer2 , setAnswer2] = useState('')
  const [fixAnswer2, setFixAnswer2] = useState('')
  const [answer3 , setAnswer3] = useState('')
  const [fixAnswer3, setFixAnswer3] = useState('')
  const [answer4 , setAnswer4] = useState('')
  const [fixAnswer4, setFixAnswer4] = useState('')
  const [disabled, setDisabled] = useState(true)

  const handleChange1  = (e) => {
    setAnswer1(e.target.value)
    setDisabled(false)
  }
  const handleChange2 = (e) => {
    setAnswer2(e.target.value)
    setDisabled(false)
  }
  const handleChange3 = (e) => {
    setAnswer3(e.target.value)
    setDisabled(false)
  }
  const handleChange4 = (e) => {
    setAnswer4(e.target.value)
    setDisabled(false)
  }

  const handleClick = () => {
    setFixAnswer1(answer1)
    setFixAnswer2(answer2)
    setFixAnswer3(answer3)
    setFixAnswer4(answer4)
    setPage(page + 1)
    if(page === 1){
      if(answer2.length < 1){
        setDisabled(true)
      }else{
        setDisabled(false)
      }
    }else if(page === 2){
      if(answer3.length < 1){
        setDisabled(true)
      }else{
        setDisabled(false)
    }
  }else{
    if(answer4.length < 1){
      setDisabled(true)
    }else{
      setDisabled(false)
    }
  }
}

  const handlePrev = () => {
    setDisabled(false)
    setPage(page - 1)
  }

  if(page === 5){
    return(
      <div className="App">
        <h1>Terima Kasih</h1>
      </div>
      )
  }


  
  return (
    <div className="App">
      <h1>Questions</h1>
      {
        page === 1 ? <Question1 value={(e) => handleChange1(e)} answer={fixAnswer1}/> : ''
      }
      {
        page === 2 ? <Question2 value={(e) => handleChange2(e)} answer={fixAnswer2}/> : ''
      }
      {
        page === 3 ? <Question3 value={(e) => handleChange3(e)} answer={fixAnswer3}/> : ''
      }
      {
        page === 4 ? <Question4 value={(e) => handleChange4(e)} answer={fixAnswer4}/> : ''
      }
    <br />
    <br />
    <br />
    {
      page > 1 ? <button onClick={() => handlePrev()} className='btn btn-primary px-5 py-3'>Prev</button> : ''
    }
      <button onClick={() => handleClick()} className={`${disabled ? 'disabled' : ''} btn btn-primary px-5 py-3`}>Next</button>
    </div>
  );
}

export default App;
