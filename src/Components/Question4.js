import React, { useState } from 'react'

export default function Question1({value, answer}) {
    const [checked1, setChecked1] = useState(answer === 'option1' ? true : false)
    const [checked2, setChecked2] = useState(answer === 'option2' ? true : false)
    const [checked3, setChecked3] = useState(answer === 'option3' ? true : false)
    const [disable, setDisable] = useState(answer ? true : false)

    const onClick1 = () => {
        setChecked1(true)
        setChecked2(false)
        setChecked3(false)
    }
    const onClick2 = () => {
        setChecked2(true)
        setChecked1(false)
        setChecked3(false)
    }
    const onClick3 = () => {
        setChecked1(false)
        setChecked2(false)
        setChecked3(true)
    }



    return (
        <div>
            <h1>apa warna dari buah alpukat</h1>
            option1 <input onChange={value} value={'option1'} disabled={disable} checked={checked1} onClick={(e) => onClick1(e)}  type="checkbox" /><br />
            option2 <input onChange={value} value={'option2'} disabled={disable} checked={checked2} onClick={(e) => onClick2(e)}  type="checkbox" /><br />
            option3 <input onChange={value} value={'option3'} disabled={disable} checked={checked3} onClick={(e) => onClick3(e)}  type="checkbox" /><br />
        </div>
    )
}
